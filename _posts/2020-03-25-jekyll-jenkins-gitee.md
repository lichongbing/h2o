---
layout: post
title: 'gitbook+jenkins+gitee实现文档持续集成'
subtitle: '文档持续集成解决方案'
date: 2020-03-25
categories: 技术
cover: 'https://image.lichongbing.com/static/d495b932e75b16853167be1b4a4bacfb.jpg'
tags: jekyll 实现文档持续集成
---
> 首先需要明确的是咱们要实现的是什么功能？是为了搭建一个gitlab+gitbook的团队协作文档系统，然后通过jenkins实现持续集成，也就是当你在gitlab上修改文档以后，jenkins会自动build此项目，这个时候你再通过浏览器访问就是修改后的内容。实现这个的过程大致分为四部分：gitbook文档编写，gitlab配置，jenkins配置，nginx实现浏览器访问。

## 一、 gitbook文档编写
### 1\. 介绍

gitbook网站是一个简单的个人在线书籍网站，在这里可以把自己的文档整理成一本书发布出来，便于阅读，gitbook提供了两套方案，1.可以直接在[gitbook官网](https://legacy.gitbook.com/)上申请账号进行创建 2.通过他们提供的命令行开发工具自己构建一个.咱们下面介绍的是第二套方案，`gitbook`命令行工具首先需要服务器上有node.js，可通过以下命令查看系统是否安装
```
node -v  #执行后又版本号输出就说明已经安装v10.9.0
```
### 2.安装gitbook-cli工具
gitbook-cli是一个在同一系统上安装和使用多个版本的GitBook的实用程序。并自动安装所需版本的GitBook来生成一本书。  
终端命令进行的安装
```
npm install gitbook-cli -g
```
执行以后如图限制则表示安装完成，可通过
```
gitbook --version
```
验证，如果提示没有此命令需要输入绝对路径，或者创建软链接.
终端第一次运行`gitbook`命令，可能会自动安装`gitbook`，因为刚才安装的是CLI，然后CLI会自动安装`gitbook`。
参考我的book.json
```
{
    "author": "lichongbing",
    "description": " 中公基础知识高频考点速记",
    "language": "zh-hans",
    "extension": null,
    "generator": "site",
    "links": {
        "sharing": {
            "all": true,
            "facebook": true,
            "google": true,
            "twitter": true,
            "weibo": true
        }
    },
    "output": null,
    "pdf": {
        "fontSize": 16,
        "margin": {
            "bottom": 36,
            "left": 62,
            "right": 62,
            "top": 36
        },
        "pageNumbers": true,
        "paperSize": "a4"
    },
    "plugins": [
        "back-to-top-button",
        "chapter-fold",
        "hide-element",
        "prism",
        "tbfed-pagefooter",
        "github",
        "anchor-navigation-ex",
        "github-buttons",
        "splitter",
        "-lunr",
        "-search",
        "search-plus",
        "-highlight",
        "-livereload",
        "openwrite",
        "theme-fexa",
        "-sharing"
    ],
    "pluginsConfig": {
        "hide-element": {
            "elements": [".gitbook-link"]
        },
        "prism": {
            "css": [
                "prismjs/themes/prism-twilight.css"
            ]
        },
        "tbfed-pagefooter": {
            "copyright":"lichongbing",
            "modify_label": "该文件最后修改时间：",
            "modify_format": "YYYY-MM-DD HH:mm:ss"
        },
        "github": {
            "url": "https://github.com/lichongbing/gfr_doc"
        },
        "anchor-navigation-ex": {
            "showLevel": true,
            "showGoTop": true
        },
        
        "github-buttons": {
            "repo": "lichongbing/gfr_doc",
            "types": [
                "star",
                "watch"
            ],
            "size": "small"
        },
        "image-captions": {
            "caption": "_CAPTION_"
        },
        
        "openwrite":{
            "blogId": "18972-1576749538667-677",
            "name": "谦谦君子",
            "qrcode": "https://image.lichongbing.com/qrcode_for_gh_29bad7d68f3a_258.jpg",
            "keyword": "更多"
        },
        "theme-fexa":{
            "search-placeholder":"输入关键字搜索",
            "logo":"./logo.png",
            "favicon": "./favicon.ico"
        },
        "sharing": {
            "douban": true,
            "facebook": true,
            "google": true,
            "pocket": true,
            "qq": true,
            "qzone": true,
            "twitter": true,
            "weibo": true,
            "all": [
                "douban",
                "facebook",
                "google",
                "instapaper",
                "linkedin",
                "twitter",
                "weibo",
                "messenger",
                "qq",
                "qzone",
                "viber",
                "whatsapp"
            ]
        }
    },
    "title": "中公基础知识高频考点速记",
    "variables": {"themeFexa":{
            "nav":[
                {
                    "url":"http://...",
                    "target":"_blank",
                    "name": "中公基础知识高频考点速记"
                }
            ]
        }}
}
```
## 二、jenkins配置
## 三、gitee配置
## 四、nginx配置
以下是gitbook+jenkins+gitee实现文档持续集成的范例
[https://gitlab.lichongbing.com](https://gitlab.lichongbing.com)